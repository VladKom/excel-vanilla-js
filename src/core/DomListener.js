import {
    capitalize
} from './utils'

export class DomListener {
    constructor($root, listeners = []) {
        if (!$root) {
            throw new Error(`no $root provided for DomListener`)
        }
        this.$root = $root
        this.listeners = listeners
    }

    initDOMListeners() {
        this.listeners.forEach(listener => {
            const method = getMethodName(listener)
            if (!this[method]) {
                const name = this.name
                throw new Error(
                    `Method ${method} is not implemented in ${name} Component`
                )
            }
            this[method] = this[method].bind(this)
            // 'on' - it's the same function 'addEventListener'
            this.$root.on(listener, this[method])
        })
    }

    removeDOMListeners() {
        this.listeners.forEach(listener => {
            const method = getMethodName(listener)
            // 'off' - it's the same function 'removeEventListener'
            this.$root.off(listener, this[method])
        })
    }
}

function getMethodName(eventName) {
    // capitalize() is for convert 'input' to 'Input'
    return 'on' + capitalize(eventName)
}