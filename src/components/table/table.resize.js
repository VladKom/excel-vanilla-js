import {
    $
} from '@core/dom'

export function resizeHandler($root, event) {
    const $resizer = $(event.target)
    // const $parent = $resizer.$el.parentNode // BAD!!!
    // const $parent = $resizer.$el.closest('.column') // better but BAD too
    const $parent = $resizer.closest('[data-type="resizable"]')
    const coords = $parent.getCoords()
    const type = $resizer.data.resize
    const sideProp = type === 'col' ? 'bottom' : 'right'
    let value

    $resizer.css({
        opacity: 1,
        [sideProp]: '-5000px'
    })
    // $root is the element from dom.js.
    // i used $root for dont parse all page
    // and down the time of render!
    // data is dataset function
    const cells = $root.findAll(`[data-col="${$parent.data.col}"]`)

    document.onmousemove = e => {
        if (type === 'col') {
            // delta - difference between edge and mouse position
            const delta = e.pageX - coords.right
            value = coords.width + delta
            $resizer.css({
                right: -delta + 'px'
            })
            // i used const cells for make fewer requests to the DOM tree
            // and only store the value once on mouse click
            // instead of thousands of requests on each move
        } else {
            const delta = e.pageY - coords.bottom
            value = coords.height + delta
            $resizer.css({
                bottom: -delta + 'px'
            })
        }
    }

    document.onmouseup = () => {
        document.onmousemove = null
        document.onmouseup = null

        if (type === 'col') {
            $parent.css({
                width: value + 'px'
            })
            cells.forEach(el => el.style.width = value + 'px')
        } else {
            $parent.css({
                height: value + 'px'
            })
        }

        $resizer.css({
            opacity: 0,
            bottom: 0,
            right: 0
        })
    }
}