# Project: Excel Pure JavaScript

##### The main goal of this project is to create a similar copy of the _Excel_ application (browser version) from _Google_ using pure javascript _without frameworks_, the project will be created based on _my mini framework_ using _object-oriented programming_, observing the rules of _clean code_.

#### Author: Vlad Kom
###### Email: komvla5@gmail.com

## Stack:

- [HTML 5]
- [CSS]
- [SCSS]
- [JavaScript (Pure)]
- [ES7]
- [Webpack]
- [Babel]
- [Eslint]
- [Git] 
- [2 Modes (Prod + Dev)]
